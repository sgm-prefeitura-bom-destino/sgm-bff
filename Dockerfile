FROM node:14-alpine
WORKDIR /usr/src/app

ADD package*.json ./

# Install dependencies.`
# RUN npm i -g @nestjs/cli
RUN npm ci --only=production
ADD ./dist ./dist


# Expose API port
EXPOSE 3000

# Run the web service on container startup
CMD [ "npm", "run", "start:prod" ]