import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { GatewayModule } from 'src/gateway/gateway.module';
import { VaultModule } from 'src/vault/vault.module';
import { AuthInternalService } from './auth-internal/auth-internal.service';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtInternalAuthGuard } from './jwt-internal-auth.guard';
import { JwtInternalStrategy } from './jwt-internal.strategy';
import { JwtModuleRegisterService } from './jwt-module-register/jwt-module-register.service';
import { JwtStrategy } from './jwt.strategy';
import { LocalInternalStrategy } from './local-internal.strategy';
import { LocalStrategy } from './local.strategy';

@Module({
  imports: [
    PassportModule,
    VaultModule, JwtModule.registerAsync({ useClass: JwtModuleRegisterService, imports: [VaultModule] }),
    forwardRef(() => GatewayModule)
  ], providers: [JwtStrategy, JwtAuthGuard, AuthService, LocalStrategy, LocalInternalStrategy,
                  JwtInternalStrategy, JwtInternalAuthGuard, AuthInternalService],
  exports: [AuthService, JwtModule, AuthInternalService]
})
export class AuthModule { }
