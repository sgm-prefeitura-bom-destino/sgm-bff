import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthInternalService } from './auth-internal/auth-internal.service';

@Injectable()
export class LocalInternalStrategy extends PassportStrategy(Strategy, 'local-internal') {
  constructor(private authService: AuthInternalService) {
    super({ usernameField: 'matricula'});
  }

  async validate(matricula: string, password: string): Promise<any> {
    const user = await this.authService.validateUser(matricula, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}