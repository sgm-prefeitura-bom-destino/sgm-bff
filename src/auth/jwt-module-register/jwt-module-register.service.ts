import { Injectable } from '@nestjs/common';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';
import { Constants } from 'src/util/constants';
import { VaultLoader } from 'src/vault/vault-loader/vault-provider.service';

@Injectable()
export class JwtModuleRegisterService implements JwtOptionsFactory {

  constructor(private vault: VaultLoader) {
  }

  async createJwtOptions(): Promise<JwtModuleOptions> {
    const secret = await this.vault.getSecret(VaultLoader.JWT_PATH, "JWT_KEY");
    return { secret, signOptions: { expiresIn: `${Constants.SESSION_MAX_DAYS}d` } };
  }
}
