import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/gateway/user/user.service';
import { VaultLoader } from 'src/vault/vault-loader/vault-provider.service';

@Injectable()
export class AuthInternalService {
  constructor(private jwtService: JwtService, private userService: UserService, private vault : VaultLoader) { }

  async validateUser(matricula: string, password: string): Promise<any> {
    const result = await this.userService.loginInternal({ matricula, password }).catch(e => {
      Logger.error(`Falha ao realizar login, matricula=${matricula}`, e, AuthInternalService.name);
      return null;
    });
    return result;
  }

  async login(user: any) {
    const secret = await this.vault.getSecret(VaultLoader.JWT_PATH, "JWT_INTERNAL_KEY");
    const payload = user;
    return this.jwtService.sign(payload, { secret });
  }
}
