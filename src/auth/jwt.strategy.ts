import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { Strategy } from 'passport-jwt';
import { Constants } from 'src/util/constants';
import { VaultLoader } from 'src/vault/vault-loader/vault-provider.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt") {
  
  constructor(private   vault : VaultLoader) {
    super({
      jwtFromRequest: extractFromCookie,
      ignoreExpiration: false,
      secretOrKeyProvider: (req, raw, done) => this.secretProvider(req, raw, done),
    });
  }

  private async secretProvider (req, rawJwtToken, done) {
    const secret = await this.vault.getSecret(VaultLoader.JWT_PATH, "JWT_KEY");
    done(null, secret);
  }

  async validate(payload: any) {    
    return payload;
  }
}

function extractFromCookie(req : Request) : string | null {
  let jwt = null;
  if (req && req.cookies) {
    jwt = req.cookies[Constants.COOKIE_NAME];
  }
  return jwt;
}