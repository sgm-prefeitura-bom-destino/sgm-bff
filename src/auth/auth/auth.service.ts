import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/gateway/user/user.service';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService, private userService: UserService) { }

  async validateUser(email: string, password: string): Promise<any> {
    const result = await this.userService.login({ email, password }).catch(e => {
      Logger.error(`Falha ao realizar login, email=${email}`, e, AuthService.name);
      return null;
    });
    return result;
  }

  login(user: any) {
    const payload = user;
    return this.jwtService.sign({...payload, _time: new Date().getTime()} );
  }
}