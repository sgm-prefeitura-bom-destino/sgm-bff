export const Constants = {
  VAULT_SETTINGS: {
    endpoint: process.env.VAULT_URL || 'http://127.0.0.1:8200',
    token: process.env.VAULT_TOKEN || 'TOKEN_DEV'
  },
  SESSION_MAX_DAYS: 1,
  COOKIE_NAME: 'session',
  COOKIE_INTERNAL_NAME: 'sessionInternal',
  GATEWAY: {
    STUR_ENDPOINT: process.env.STUR_ENDPOINT || "http://localhost:3001",
    USERS_ENDPOINT: process.env.USERS_ENDPOINT || "http://localhost:3002",
    SASCI_ENDPOINT: process.env.SASCI_ENDPOINT || "http://localhost:3003",
  }
}