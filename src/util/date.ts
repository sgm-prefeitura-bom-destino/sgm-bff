export const addDays = (date: Date, days: number) => {
  let result = new Date(date);
  result.setDate(date.getDate() + days);
  return result;
}

export const addHours = (date: Date, hours : number) => {
  let result = new Date(date);
  result.setHours(date.getHours() + hours);
  return result;
}

export const safeAddMonths = (date: Date, months: number) => {
  const month = new Date(date);
  month.setDate(1);
  month.setMonth(month.getMonth() + months);
  return setDay(month, date.getDate());
}

export const atStartOf = (date: Date, day: number = date.getDate(), month: number = date.getMonth()) => {
  const result = new Date(date);
  result.setDate(1);
  result.setHours(0, 0, 0, 0);
  result.setMonth(month);
  result.setDate(day);
  return result;
}

export const getLastDayOfMonth = (date: Date): number => {
  const currentMonth = atStartOf(date, 1);
  currentMonth.setMonth(currentMonth.getMonth() + 1, 0);
  return currentMonth.getDate();
}

/**
 * Seta a data de referência pro mes informado, respeitando o ultimo dia do mes
 * @param date mes/ano de referencia (dia não importa)
 * @param referDay dia de referencia
 */
export const setDay = (date: Date, referDay: number): Date => {
  const lastDay = getLastDayOfMonth(date);
  const result = new Date(date);
  result.setDate(Math.min(referDay, lastDay));
  return result;
}

/**
 * Considera o timezone da data informada como UTC e converte para o timezone local
 * @param date data a ser convertida
 */
export const useInUTC = (date: Date): Date => {
  const [year, month, day, hour, minutes, seconds, ms] = [
    date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 
    date.getSeconds(), date.getMilliseconds()
  ];
  return new Date(Date.UTC(year, month, day, hour, minutes, seconds, ms));
}

/**
 * Converte a data informada para o seu horario em UTC
 * @param date 
 */
export const toUTC = (date : Date) : Date => {
  return atTimezone(date, 0);
}

export const atTimezone = (date : Date, timezoneOffset : number) : Date => {
  const systemTimezone = new Date().getTimezoneOffset();
  return addHours(date, (systemTimezone - timezoneOffset) / 60);
}