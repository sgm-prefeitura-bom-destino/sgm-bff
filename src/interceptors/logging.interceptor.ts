import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common';
import { Request } from 'express';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Constants } from 'src/util/constants';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const initialTime = new Date().getTime();
    const req: Request = context.switchToHttp().getRequest();
    const params = JSON.stringify(req.params);
    const query = JSON.stringify(req.query);
    const session : string = (req.cookies[Constants.COOKIE_NAME] ?? "");
    const cookie = session.substring(Math.max(session.length - 7, 0), session.length);
    const path = req.path;

    return next.handle().pipe(
      tap(() => {
        const duration = new Date().getTime() - initialTime;
        Logger.log(`path=${path}, params=${params}, query=${query}, cookie=${cookie}, duration=${duration}ms`, context.getClass().name, false)
      }),
      catchError((e) => {
        const duration = new Date().getTime() - initialTime;
        Logger.error(`path=${path} , params=${params}, query=${query}, cookie=${cookie}, duration=${duration}ms`, e, context.getClass().name);
        return throwError(e);
      }));
  }
}
