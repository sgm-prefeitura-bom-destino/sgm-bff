import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

const origin = (process.env.ORIGIN || 'http://localhost:3001').split(',');
const port = process.env.PORT || '3000';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({ origin, credentials: true });
  app.use(cookieParser());
  await app.listen(Number(port));
}
bootstrap();
