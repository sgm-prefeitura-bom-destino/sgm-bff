import { HttpService, Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Constants } from 'src/util/constants';
import { GenericGatewayService } from '../generic-gateway.service';

@Injectable({ scope: Scope.REQUEST})
export class CidadaoService extends GenericGatewayService {

  constructor(@Inject(REQUEST) req : Request, private http : HttpService) {
    super(req);
  }

  updateUser(user) : Promise<any> {
    return this.http.put(`${Constants.GATEWAY.USERS_ENDPOINT}/user`, user, {
      headers: super.getDefaultHeaders(),
    }).toPromise().then(a => a.data);
  }

}
