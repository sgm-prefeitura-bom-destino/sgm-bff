import { Request } from "express";
import { Constants } from "src/util/constants";

export class GenericGatewayService {

  constructor(private request : Request) {}

  protected getDefaultHeaders(cookieName = Constants.COOKIE_NAME) {
    const authorization = `Bearer ${this.request.cookies[cookieName]}`;
    return { authorization };
  }

}
