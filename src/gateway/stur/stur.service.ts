import { HttpService, Inject, Injectable, Logger, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from 'src/util/constants';
import { GenericGatewayService } from '../generic-gateway.service';
import { AxiosResponse } from 'axios';

@Injectable({
  scope: Scope.REQUEST
})
export class SturService extends GenericGatewayService {

  private readonly logger = new Logger(SturService.name);

  constructor(@Inject(REQUEST) request: Request, private http: HttpService) {
    super(request);
  }

  fetchIPTU(year): Promise<any> {
    return this.http.get(`${Constants.GATEWAY.STUR_ENDPOINT}/iptu`, {
      params: { year },
      headers: super.getDefaultHeaders()
    }).toPromise().then(a => a.data);
  }

  fetchITR(year): Promise<any> {
    return this.http.get(`${Constants.GATEWAY.STUR_ENDPOINT}/itr`, {
      params: { year },
      headers: super.getDefaultHeaders()
    }).toPromise().then(a => a.data);
  }

}
