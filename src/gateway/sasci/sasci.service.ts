import { HttpService, Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Constants } from 'src/util/constants';
import { GenericGatewayService } from '../generic-gateway.service';

@Injectable({
  scope: Scope.REQUEST
})
export class SasciService extends GenericGatewayService {

  constructor(@Inject(REQUEST) req: Request, private http: HttpService) {
    super(req);
  }

  saveAppointment() {
    return this.http.post(`${Constants.GATEWAY.SASCI_ENDPOINT}/sasci/appointment`, null, {
      headers: super.getDefaultHeaders()
    }).toPromise()
      .then(a => a.data);
  }

  fetchAppointments() {
    return this.http.get(`${Constants.GATEWAY.SASCI_ENDPOINT}/sasci/appointment`, {
      headers: super.getDefaultHeaders()
    }).toPromise()
      .then(a => a.data);
  }

  reeschedule() {
    return this.http.put(`${Constants.GATEWAY.SASCI_ENDPOINT}/sasci/appointment/reschedule`, null, {
      headers: super.getDefaultHeaders()
    }).toPromise()
      .then(a => a.data);
  }

  cancel() {
    return this.http.put(`${Constants.GATEWAY.SASCI_ENDPOINT}/sasci/appointment/cancel`, null, {
      headers: super.getDefaultHeaders()
    }).toPromise()
      .then(a => a.data);
  }

  fetchDoctor(postoId, date) {
    return this.http.get(`${Constants.GATEWAY.SASCI_ENDPOINT}/sasci/doctor`, {
      params: { postoId, date },
      headers: super.getDefaultHeaders()
    }).toPromise()
      .then(a => a.data);
  }

  internalApprove() {
    return this.http.put(`${Constants.GATEWAY.SASCI_ENDPOINT}/internal/sasci/appointment/approve`, null, {
      headers: super.getDefaultHeaders(Constants.COOKIE_INTERNAL_NAME)
    }).toPromise()
      .then(a => a.data);
  }

  internalCancel() {
    return this.http.put(`${Constants.GATEWAY.SASCI_ENDPOINT}/internal/sasci/appointment/cancel`, null, {
      headers: super.getDefaultHeaders(Constants.COOKIE_INTERNAL_NAME)
    }).toPromise()
      .then(a => a.data);
  }

  internalFetchAppointments() {
    return this.http.get(`${Constants.GATEWAY.SASCI_ENDPOINT}/internal/sasci/appointment`, {
      headers: super.getDefaultHeaders(Constants.COOKIE_INTERNAL_NAME)
    }).toPromise()
      .then(a => a.data);
  }





}
