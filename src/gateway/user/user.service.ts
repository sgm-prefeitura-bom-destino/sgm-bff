import { HttpService, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Constants } from 'src/util/constants';

@Injectable()
export class UserService {

  constructor(private http : HttpService, private jwt : JwtService) {
    // super(req);
  }

  saveUser(user) : Promise<any> {
    const jwt = this.jwt.sign(user);
    return this.http.post(`${Constants.GATEWAY.USERS_ENDPOINT}/user`, user, {
      headers: { Authorization: `Bearer ${jwt}`},
    }).toPromise().then(a => a.data);
  }

  login(user) : Promise<any> {
    const jwt = this.jwt.sign(user);
    return this.http.post(`${Constants.GATEWAY.USERS_ENDPOINT}/user/login`, user, {
      headers: { Authorization: `Bearer ${jwt}`},
    }).toPromise().then(a => a.data);
  }
  
  loginInternal(user) : Promise<any> {
    const jwt = this.jwt.sign(user);
    return this.http.post(`${Constants.GATEWAY.USERS_ENDPOINT}/user-internal/login`, user, {
      headers: { Authorization: `Bearer ${jwt}`},
    }).toPromise().then(a => a.data);
  }

}
