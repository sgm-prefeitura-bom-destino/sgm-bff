import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { SturService } from './stur/stur.service';
import { UserService } from './user/user.service';
import { CidadaoService } from './cidadao/cidadao.service';
import { SasciService } from './sasci/sasci.service';

@Module({
  imports: [HttpModule, forwardRef(() => AuthModule)],
  providers: [SturService, UserService, CidadaoService, SasciService],
  exports: [SturService, UserService, CidadaoService, SasciService]
})
export class GatewayModule {}
