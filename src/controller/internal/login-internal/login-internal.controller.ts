import { Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthInternalService } from 'src/auth/auth-internal/auth-internal.service';
import { Constants } from 'src/util/constants';
import { addDays } from 'src/util/date';

@Controller('internal')
export class LoginInternalController {
  constructor(private authService: AuthInternalService) { }


  @UseGuards(AuthGuard('local-internal'))
  @Post('/login')
  async login(@Req() req: Request) {
    const jwt = await this.authService.login(req.user);
    req.res.cookie(Constants.COOKIE_INTERNAL_NAME, jwt, { expires: addDays(new Date(), Constants.SESSION_MAX_DAYS) });
    return req.user;
  }

}
