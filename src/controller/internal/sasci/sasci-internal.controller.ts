import { Controller, Get, HttpException, Put, Req, UnauthorizedException, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { JwtInternalAuthGuard } from 'src/auth/jwt-internal-auth.guard';
import { SasciService } from 'src/gateway/sasci/sasci.service';

@Controller('internal/sasci')
@UseGuards(JwtInternalAuthGuard)
export class SasciInternalController {

  constructor(private readonly sasci: SasciService) {

  }

  @Put('/appointment/cancel')
  cancel() {
    return this.sasci.internalCancel().catch(e => {
      console.log("erro", e)
      throw e;
    });
  }

  @Put('/appointment/approve')
  approve() {
    return this.sasci.internalApprove()
  }

  @Get('/appointment')
  fetchScheduledAppointments(@Req() req: Request) {
    return this.sasci.internalFetchAppointments().catch(e => {
      throw new HttpException(e.response?.data?.message, e.response.status);
    });
  }

}
