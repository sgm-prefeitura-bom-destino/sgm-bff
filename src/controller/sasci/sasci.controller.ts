import { Controller, Get, HttpException, Post, Put, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SasciService } from 'src/gateway/sasci/sasci.service';

@Controller('sasci')
@UseGuards(JwtAuthGuard)
export class SasciController {

  constructor(private readonly sasci : SasciService) {

  }

  @Post('/appointment')
  saveAppointment() {
    return this.sasci.saveAppointment();
  }
  
  @Get('/appointment')
  fetchAppointments() {
    return this.sasci.fetchAppointments();
  }

  @Put('/appointment/reschedule')
  reschedule() {
    return this.sasci.reeschedule();
  }
  
  @Put('/appointment/cancel')
  cancel() {
    return this.sasci.cancel();
  }

  @Get('/doctor')
  fetchDoctor(@Query('postoId') postoId: string, @Query('date') date: string) {
    return this.sasci.fetchDoctor(postoId, date).catch(e => {
      throw new HttpException(e.response?.data?.message, e.response?.status ?? 500);
    });
  }

}
