import { BadRequestException, Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthService } from 'src/auth/auth/auth.service';
import { UserService } from 'src/gateway/user/user.service';
import { Constants } from 'src/util/constants';
import { addDays } from 'src/util/date';

@Controller()
export class LoginController {
  constructor(private authService: AuthService, private readonly userService : UserService) { }


  @UseGuards(AuthGuard('local'))
  @Post('/login')
  async login(@Req() req: Request) {
    const jwt = this.authService.login(req.user);
    req.res.cookie(Constants.COOKIE_NAME, jwt, { expires: addDays(new Date(), Constants.SESSION_MAX_DAYS) });
    return req.user;
  }
  
  @Post('/register')
  async register(@Body() user) {
    return this.userService.saveUser(user).catch(() => {
      throw new BadRequestException();
    });
  }

  @Post('/logout')
  async logout(@Req() req: Request) {
    req.res.clearCookie(Constants.COOKIE_NAME);
  }

}
