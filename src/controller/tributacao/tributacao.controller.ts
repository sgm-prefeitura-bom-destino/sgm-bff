import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SturService } from 'src/gateway/stur/stur.service';

@Controller('tributacao')
@UseGuards(JwtAuthGuard)
export class TributacaoController {

  constructor(private sturGateway : SturService) {}

  @Get('/iptu')
  fetchIPTU(@Query('year') year) {
    return this.sturGateway.fetchIPTU(year);
  }
  
  @Get('/itr')
  fetchITR(@Query('year') year) {
    return this.sturGateway.fetchITR(year);
  }

}
