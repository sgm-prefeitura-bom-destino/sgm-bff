import { Body, Controller, Get, Put, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CidadaoService } from 'src/gateway/cidadao/cidadao.service';

@Controller('cidadao')
@UseGuards(JwtAuthGuard)
export class CidadaoController {

  constructor(private readonly cidadaoService : CidadaoService) {
  }

  @Get()
  getInfo(@Req() req : Request) {
    return req.user;
  }

  @Put()
  updateUser(@Body() user) {
    return this.cidadaoService.updateUser(user);
  }

}
