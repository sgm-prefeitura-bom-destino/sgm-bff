import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { LoginController } from './controller/login/login.controller';
import { TributacaoController } from './controller/tributacao/tributacao.controller';
import { GatewayModule } from './gateway/gateway.module';
import { VaultModule } from './vault/vault.module';
import { CidadaoController } from './controller/cidadao/cidadao.controller';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { LoginInternalController } from './controller/internal/login-internal/login-internal.controller';
import { SasciInternalController } from './controller/internal/sasci/sasci-internal.controller';
import { SasciController } from './controller/sasci/sasci.controller';

@Module({
  imports: [VaultModule, AuthModule, GatewayModule],
  controllers: [LoginController, TributacaoController, LoginInternalController, CidadaoController, SasciInternalController, SasciController],
  providers: [{
    provide: APP_INTERCEPTOR,
    useClass: LoggingInterceptor
  }],
})
export class AppModule { }
